﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.Mappings
{
    public class PersonMap : ClassMap<src.Person>
    {
        public PersonMap()
        {
            Id(x => x.ID);
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.BirthName);
            Map(x => x.Birth);
            Map(x => x.Death);
            Map(x => x.Place);
            Map(x => x.isMale);
            Map(x => x.isDead);
            Map(x => x.InfoText);
            References(x => x.Parent);  
            References(x => x.Spouse);
            Map(x => x.PanelPosition);

            HasManyToMany<src.Document>(x => x.Documents).Table("PersonDocument");
        }
    }
}
