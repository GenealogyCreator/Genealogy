﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GenealogyCreator.src;
using ColorHelper;
using GedNet;

namespace GenealogyCreator.Controls
{
    /// <summary>
    /// Interaction logic for PersonPanel.xaml
    /// </summary>
    public partial class PersonPanel : UserControl
    {
        src.Person itsPerson;
        ToolTip tooltip;
        public Guid ID { get; set; }
        public Dictionary<src.Line, src.Line.LineTarget> Lines;
        public bool Selected { get; set; }
        

        public PersonPanel()
        {
            InitializeComponent();
            Lines = new Dictionary<src.Line, src.Line.LineTarget>();
            tooltip = new System.Windows.Controls.ToolTip();
            this.ToolTip = tooltip;
            Selected = false;
            ID = Guid.NewGuid();
            PnlBorder.BorderBrush = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderUnselectedColor));
        }

        public PersonPanel(src.Person p)
        {
            InitializeComponent();
            Lines = new Dictionary<src.Line, src.Line.LineTarget>();
            tooltip = new System.Windows.Controls.ToolTip();
            this.ToolTip = tooltip;
            Selected = false;
            ID = Guid.NewGuid();

            itsPerson = p;
            writePersonData();

            if(p.PanelPosition.Left == 0 && p.PanelPosition.Top == 0)
            {
                Random rand = new Random();
                this.Margin = new Thickness(100, 100, 0, 0);
                p.PanelPosition = this.Margin;
            }
            else
                this.Margin = p.PanelPosition;

            PnlBorder.BorderBrush = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderUnselectedColor));
            PnlBorder.BorderThickness = new Thickness(2);

            if (itsPerson.isMale)
                PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Unselected));
            else
                PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Unselected));
        }

        private void writePersonData()
        {
            personName.Text = itsPerson.FirstName + " " + itsPerson.LastName;

            StringBuilder PersonInfoTxt = new StringBuilder();

            if (itsPerson.BirthName != null && itsPerson.BirthName.Length > 0)
                PersonInfoTxt.Append("( " + itsPerson.BirthName + " )\n");


            EVENT birth = itsPerson.getEvent(GedNet.EVENT.TYPES.BIRTH);
            if(birth != null)
                PersonInfoTxt.Append("*" + birth.Date + " " + birth.Place + "\n");

            EVENT death = itsPerson.getEvent(GedNet.EVENT.TYPES.DEATH);
            if (death != null)
                PersonInfoTxt.Append(" - " + death.Date + " " + death.Place + "\n");

            personInfo.Text = PersonInfoTxt.ToString();
            tooltip.Content = itsPerson.FirstName + " " + itsPerson.LastName + "\n" + PersonInfoTxt.ToString();

            if (itsPerson.isMale)
                PnlBorder.Background = new SolidColorBrush(Color.FromRgb(170, 215, 250));
            else
                PnlBorder.Background = new SolidColorBrush(Color.FromRgb(250, 215, 170));
        }

        internal void updateMargin()
        {
            itsPerson.PanelPosition = this.Margin;
        }

        internal void select(src.Line.LineTarget targets = src.Line.LineTarget.All)
        {
            PnlBorder.BorderBrush = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderSelectedColor));

            if (itsPerson.isMale)
                PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Selected));
            else
                PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Selected));

            this.BorderThickness = new Thickness(2);

            Selected = true;

            if (targets == src.Line.LineTarget.All)
            {
                foreach (var L in Lines.Keys)
                    L.select();
            }
            else
            {
                foreach (var L in Lines)
                {
                    if(L.Value == targets)
                        L.Key.select();
                }
            }

        }


        internal void unselect()
        {
            PnlBorder.BorderBrush = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderUnselectedColor));
            this.BorderThickness = new Thickness(2);
            Selected = false;
            foreach (var L in Lines.Keys)
                L.unselect();


            if (itsPerson.isMale)
                PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Unselected));
            else
                PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Unselected));
        }

        internal Person getPerson()
        {
            return itsPerson;
        }

        internal void updatePersonData()
        {
            writePersonData();
        }

        internal void addLine(ref src.Line l, src.Line.LineTarget target)
        {
            if(!Lines.ContainsKey(l))
                Lines.Add(l, target);
        }

        internal void updateLines(Thickness newPos, Thickness oldPos)
        {
            foreach (src.Line l in Lines.Keys)
            {
                l.updatePosition(newPos, oldPos);
            }
        }

        internal void updatePosition(Thickness t)
        {
            updateLines(t, Margin);
            this.Margin = t;
            updateMargin();
        }

        internal bool LineExists(Guid iD)
        {
            foreach (src.Line l in Lines.Keys)
            {
                if (l.isInbetween(iD, itsPerson.ID))
                    return true;
            }

            return false;
        }


        public override string ToString()
        {
            return "PPanel: " + personName.Text;
        }

        internal void Offset(Vector Offset, double ZoomFactor, Thickness startMargin)
        {
            Thickness newPos = startMargin;
            newPos.Top -= (Offset.Y) * (1 / ZoomFactor);
            newPos.Left -= (Offset.X) * (1 / ZoomFactor);
            updatePosition(newPos);
        }

        internal void updateColor()
        {
            foreach (src.Line l in Lines.Keys)
                l.updateColor();

            if (Selected)
            {
                PnlBorder.BorderBrush = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderSelectedColor));

                if (itsPerson.isMale)
                    PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Selected));
                else
                    PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Selected));
            }
            else
            {
                PnlBorder.BorderBrush = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderUnselectedColor));

                if (itsPerson.isMale)
                    PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Unselected));
                else
                    PnlBorder.Background = new SolidColorBrush(ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Unselected));
            }

        }

        internal void UpdateLine()
        {
            foreach (src.Line l in Lines.Keys)
                l.UpdateLine();
        }
    }
}
