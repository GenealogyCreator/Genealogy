﻿using GenealogyCreator.src;
using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace GenealogyCreator.Controls
{
    /// <summary>
    /// Interaction logic for SpousePanel.xaml
    /// </summary>
    public partial class SpousePanel : UserControl
    {
        public event EventHandler<string> FamilyLinkDoubleClick;
        // public event EventHandler<Person> HusbandDoubleClick;
        // public event EventHandler<Person> WifeDoubleClick
        public event EventHandler<Person> PersonDoubleClick;
        public event EventHandler<AddPersonEventArgs> OnAddPersonClick;
        public Person refPerson { get; set; }


        public SpousePanel()
        {
            InitializeComponent();
        }

        internal void Clear()
        {
            HusbandPanel.Clear();
            WifePanel.Clear();
            FamilyLink.Content = "";
        }

        private void FamilyLink_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FamilyLinkDoubleClick?.Invoke(this, this.FamilyLink.Content.ToString());
        }

        private void HusbandPanel_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PersonDoubleClick?.Invoke(this, HusbandPanel.itsPerson);
        }

        private void WifePanel_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            PersonDoubleClick?.Invoke(this, WifePanel.itsPerson);
        }

        private void HusbandPanel_OnAddPersonClick(object sender, EventArgs e)
        {
            OnAddPersonClick?.Invoke(this, new AddPersonEventArgs(refPerson, Family.Relationships.Father));
        }

        private void WifePanel_OnAddPersonClick(object sender, EventArgs e)
        {
            OnAddPersonClick?.Invoke(this, new AddPersonEventArgs(refPerson, Family.Relationships.Mother));
        }
    }

    public class AddPersonEventArgs : EventArgs
    {
        public Person refPerson;
        public Family.Relationships relation;

        public AddPersonEventArgs(Person p, Family.Relationships r)
        {
            relation = r;
            refPerson = p;
        }
    }
}
