﻿using GenealogyCreator.src;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace GenealogyCreator.Controls
{
    /// <summary>
    /// Interaction logic for PersonLink.xaml
    /// </summary>
    public partial class PersonLinkControl : UserControl
    {

        private Point ActualPos;
        private Size ActualSize;
        public Guid ID = Guid.NewGuid();
        private Point InitialOffset = new Point(52, 56);
        private Size InitialBoxSize = new Size(80, 80);
        private Size InitialControlSize = new Size(200, 140);

        public PersonLink Link { get; set; }

        private List<Person> searchResult;            
        public event EventHandler OnDeleteClick;
        public event EventHandler OnSaveClick;
        private GenealogyContext db;

        public Person PersonLinked {
            get { return Link.PersonLinked; }
            set { Link.PersonLinked = value; }
        }

        public MediaObject Image
        {
            get { return Link.Image; }
            set { Link.Image = value; }
        }

        public PersonLinkControl(PersonLink pl, ref GenealogyContext gdb)
        {
            InitializeComponent();
            Link = pl;
            db = gdb;
            lbl_PersonsName.Content = Link.PersonLinked.ToString();
            searchResult = new List<Person>();
            this.VerticalAlignment = VerticalAlignment.Top;
            this.HorizontalAlignment = HorizontalAlignment.Left;  

            InputGrid.Visibility = Visibility.Collapsed;
            DisplayGrid.Visibility = Visibility.Collapsed;

            tb_PersonsName.Text = Link.PersonLinked.FirstName;
            cb_SearchResult.SelectedItem = Link.PersonLinked;
        }

        public PersonLinkControl(ref GenealogyContext gdb)
        {
            InitializeComponent();
            Link = new PersonLink();
            db = gdb;
            searchResult = new List<Person>();
            this.VerticalAlignment = VerticalAlignment.Top;
            this.HorizontalAlignment = HorizontalAlignment.Left; 

            InputGrid.Visibility = Visibility.Collapsed;
            DisplayGrid.Visibility = Visibility.Collapsed;
        }

        public override string ToString()
        {
            return PersonLinked.ToString();
        }

        public void Highlight()
        {
            LinkBox.BorderBrush = new SolidColorBrush(Colors.Aqua);
            DisplayGrid.Visibility = Visibility.Visible;
        }

        public void UnHighlight()
        {
            LinkBox.BorderBrush = new SolidColorBrush(Colors.Gray);
            DisplayGrid.Visibility = Visibility.Collapsed;
        }


        public void setPosition(Point s)
        {
            this.Margin = new Thickness(s.X - (LinkBox.Width / 2), s.Y - (LinkBox.Height / 2), 0, 0);
        }

        private void UserControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Highlight();
        }

        private void UserControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            UnHighlight();
        }

        internal void setSize(Size frameSize, double imgScale)
        {
            ActualSize = frameSize;

            Link.InitialSize = new Coordinates(frameSize.Width * (100 / imgScale), frameSize.Height * (100 / imgScale));

            double newHeight = ActualSize.Height + InitialOffset.Y;
            if(newHeight> InitialControlSize.Height)
                this.Height = newHeight;

            double newWidth = ActualSize.Width + InitialOffset.X;
            if (newWidth > InitialControlSize.Width)
                this.Width = newHeight;
            

            LinkBox.Height = ActualSize.Height;
            LinkBox.Width = ActualSize.Width;
        }

        internal void Offset(double x, double y, double scale)
        {
            //Size
            ActualSize.Width = Link.InitialSize.Width / (100 / scale);
            ActualSize.Height = Link.InitialSize.Height / (100 / scale);

            double newHeight = ActualSize.Height + InitialOffset.Y+5;
            if (newHeight > InitialControlSize.Height)
                this.Height = newHeight;

            double newWidth = ActualSize.Width + InitialOffset.X;
            if (newWidth > InitialControlSize.Width)
                this.Width = newWidth;

            LinkBox.Height = ActualSize.Height;
            LinkBox.Width = ActualSize.Width;

            //Position
            double xPos = Link.OriginPos.X / (100 / scale);
            if (!Double.IsNaN(x))
                xPos += x;
          //  xPos -= (ActualSize.Width / 2);
            xPos -= InitialOffset.X;

            double yPos = Link.OriginPos.Y / (100 / scale);
            if(!Double.IsNaN(y))
                yPos += y;
       //     yPos -= (ActualSize.Height / 2);
            yPos -= InitialOffset.Y;

            ActualPos = new Point(xPos, yPos);

            this.Margin = new Thickness(ActualPos.X, ActualPos.Y, 0, 0);



        }

        internal void setOrigin(Point mousePosImg, double imgScale)
        {
            Link.OriginPos = new Coordinates(mousePosImg.X * (100 / imgScale), mousePosImg.Y * (100 / imgScale));
        }

        private void LinkBox_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (InputGrid.Visibility == Visibility.Visible)
            {
                InputGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                InputGrid.Visibility = Visibility.Visible;
            }
        }

        private void tb_PersonsName_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = tb_PersonsName.Text;
            searchResult.Clear();
         //   searchResult = personList.Values.Where(p => p.FirstName.Contains(searchText)).ToList();
            searchResult = db.People.Where(p => p.FirstName.Contains(searchText)).ToList(); //Doesnt Work, things dont get saved
            cb_SearchResult.ItemsSource = searchResult;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            //Todo 
            if (cb_SearchResult.SelectedIndex >= 0)
            {
                Link.PersonLinked = cb_SearchResult.SelectedItem as Person;
                lbl_PersonsName.Content = Link.PersonLinked.ToString();
                InputGrid.Visibility = Visibility.Collapsed;
                Link.PersonLinked.AddLink(this.Link);
                OnSaveClick?.Invoke(this, EventArgs.Empty);
            }
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            OnDeleteClick?.Invoke(this, EventArgs.Empty);
        }
    }
}
