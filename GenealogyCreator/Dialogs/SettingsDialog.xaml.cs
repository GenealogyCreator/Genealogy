﻿using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Forms;

namespace GenealogyCreator.Dialogs
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsDialog : Window
    {
        private ResourceManager rm = new ResourceManager("GenealogyCreator.Properties.Resources", Assembly.GetExecutingAssembly());
        public bool dbChanged = false;
        public SettingsDialog()
        {
            InitializeComponent();
            tbDatabaseFile.Text = new src.DBBackupHandler().getCurrentDbFile();
        }

        private void btn_mediaDirectory_selectPath_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.SelectedPath = Properties.Settings.Default.mediaDirectory;
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if(result == System.Windows.Forms.DialogResult.OK)
                {
                    Properties.Settings.Default.mediaDirectory = dialog.SelectedPath;
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void btn_Database_selectPath_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Multiselect = false;
            ofd.InitialDirectory = System.IO.Path.GetFullPath(new src.DBBackupHandler().getCurrentDbFile());
            ofd.Filter = rm.GetString("GenealogyDatabase") + " (.gdb)|*.gdb"; // Filter files by extension

            ofd.ShowDialog();

            if (ofd.FileName != null && ofd.FileName.Length > 0)
            {
                string connectionString = "Data Source=" + ofd.FileName + ";";
                tbDatabaseFile.Text = ofd.FileName;
                dbChanged = true;
                Properties.Settings.Default.db_connection_string = connectionString;
                Properties.Settings.Default.Save();
            }
        }
    }
}
