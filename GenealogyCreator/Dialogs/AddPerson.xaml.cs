﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GenealogyCreator.Controls;

namespace GenealogyCreator.Dialogs
{
    /// <summary>
    /// Interaction logic for AddPerson.xaml
    /// </summary>
    public partial class AddPerson : Window
    {
        private AddPersonEventArgs e;

        public AddPerson()
        {
            InitializeComponent();
        }

        public AddPerson(AddPersonEventArgs e)
        {
            this.e = e;
        }
    }
}
