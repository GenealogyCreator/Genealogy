﻿using GenealogyCreator.Controls;
using GenealogyCreator.src;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GenealogyCreator.Dialogs
{
    /// <summary>
    /// Interaction logic for FIleViewer.xaml
    /// </summary>
    public partial class FileViewer : Window
    {
        private MediaObject ImageObject;
        private ObservableCollection<PersonLinkControl> Links;
        Person selectedPerson;
        private Size OriginalImageSize;
        private Point dragStart;
        private Point dragStartGrid;
        private bool drangNDropStarted = false; 
        private GenealogyContext db;
        public FileViewer()
        {
            InitializeComponent();
        }
        

        public FileViewer(MediaObject o, Person selectedPerson, ref GenealogyContext gdb)
        {
            InitializeComponent();
            Links = new ObservableCollection<PersonLinkControl>();
            db = gdb;
            string FilePath = System.IO.Path.Combine(Properties.Settings.Default.mediaDirectory, o.FILE);
            BitmapImage i = new BitmapImage(new Uri(FilePath));
            OriginalImageSize.Width = i.PixelWidth;
            OriginalImageSize.Height = i.PixelHeight;
            image.Source = i;

            this.ImageObject = o;
            this.selectedPerson = selectedPerson;
            DataGrid.DataContext = o;
            
            foreach (PersonLink pl in o.Links)
                initLink(pl);

        }

        private void image_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!drangNDropStarted)
                return;

            Point mousePos = e.GetPosition(this.image);
            Point mousePosGrid = e.GetPosition(this.imageGrid);

            double imgScale = (100 / OriginalImageSize.Width) * image.ActualHeight;
            double xOffs = (imageGrid.ActualWidth - image.ActualWidth)/2;
            double yOffs = (imageGrid.ActualHeight - image.ActualHeight)/ 2;

            Size frameSize = new Size(Math.Abs(dragStart.X- mousePos.X), Math.Abs(dragStart.Y - mousePos.Y));

            if (frameSize.Width < 10 || frameSize.Height < 10)
            {
                frameSize.Width = 80;
                frameSize.Height = 80;
                dragStart.X = mousePos.X-40;
                dragStart.Y = mousePos.Y - 40;
            }

            PersonLinkControl l = new PersonLinkControl(ref db);
            l.PersonLinked = selectedPerson;
            l.setSize(frameSize, imgScale);
            l.setOrigin(dragStart, imgScale);
            l.Offset(xOffs, yOffs, imgScale);
            l.OnSaveClick += L_OnSaveClick;
            l.OnDeleteClick += L_OnDeleteClick;
            l.Image = ImageObject;
            LinkedPersons.Items.Add(new KeyValuePair<Guid, string>(l.ID, l.ToString()));

            ImageObject.Links.Add(l.Link);
            imageGrid.Children.Add(l);
            Links.Add(l);

            selectionBox.Visibility = Visibility.Collapsed;
            drangNDropStarted = false;
        }

        private void initLink(PersonLink pl)
        {
            PersonLinkControl l = new PersonLinkControl(pl, ref db);

            double imgScale = (100 / OriginalImageSize.Width) * image.ActualHeight;
            double xOffs = (imageGrid.ActualWidth - image.ActualWidth) / 2;
            double yOffs = (imageGrid.ActualHeight - image.ActualHeight) / 2;

            l.Offset(xOffs, yOffs, imgScale);
            l.OnDeleteClick += L_OnDeleteClick;
            l.OnSaveClick += L_OnSaveClick;
            
            imageGrid.Children.Add(l);
            Links.Add(l);
            LinkedPersons.Items.Add(new KeyValuePair<Guid, string>(l.ID, l.ToString()));
        }

        private void L_OnSaveClick(object sender, EventArgs e)
        {
            PersonLinkControl l = sender as PersonLinkControl;
            var kvp = new KeyValuePair<Guid, string>(l.ID, l.ToString());

            foreach (var item in LinkedPersons.Items)
            {
                if( ((KeyValuePair<Guid, string>)item).Key == kvp.Key )
                {
                    LinkedPersons.Items.Remove(item);
                    LinkedPersons.Items.Add(kvp);
                    return;
                }
            }
        }


        private void L_OnDeleteClick(object sender, EventArgs e)
        {
            PersonLinkControl pl = sender as PersonLinkControl;
            imageGrid.Children.Remove(pl);
            Links.Remove(pl);
            ImageObject.Links.Remove(pl.Link);
            pl.PersonLinked.RemoveLink(pl.Link);

            LinkedPersons.Items.Remove(new KeyValuePair<Guid, string>(pl.ID, pl.PersonLinked.ToString()));
            
            pl = null;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double imgScale = (100 / OriginalImageSize.Width) * image.ActualHeight;
            double xOffs = (imageGrid.ActualWidth - image.ActualWidth) / 2;
            double yOffs = (imageGrid.ActualHeight - image.ActualHeight) / 2;

            foreach (var l in Links)
                l.Offset(xOffs, yOffs, imgScale);
        }

        private void image_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            drangNDropStarted = true;
            dragStart = e.GetPosition(this.image);
            dragStartGrid = e.GetPosition(this.imageGrid);

            // Initial placement of the drag selection box.         
            Canvas.SetLeft(selectionBox, dragStartGrid.X);
            Canvas.SetTop(selectionBox, dragStartGrid.Y);
            selectionBox.Width = 0;
            selectionBox.Height = 0;

            // Make the drag selection box visible.
            selectionBox.Visibility = Visibility.Visible;
        }

        private void image_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point mousePos = e.GetPosition(imageGrid);

                if (dragStartGrid.X < mousePos.X)
                {
                    Canvas.SetLeft(selectionBox, dragStartGrid.X);
                    selectionBox.Width = mousePos.X - dragStartGrid.X;
                }
                else
                {
                    Canvas.SetLeft(selectionBox, mousePos.X);
                    selectionBox.Width = dragStartGrid.X - mousePos.X;
                }

                if (dragStartGrid.Y < mousePos.Y)
                {
                    Canvas.SetTop(selectionBox, dragStartGrid.Y);
                    selectionBox.Height = mousePos.Y - dragStartGrid.Y;
                }
                else
                {
                    Canvas.SetTop(selectionBox, mousePos.Y);
                    selectionBox.Height = dragStartGrid.Y - mousePos.Y;
                }
            }
        }

        private void LinkedPersons_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var i = (sender as ListBox).SelectedItem;

            if (i == null)
                return;

            KeyValuePair<Guid, string> pl = (KeyValuePair<Guid, string>)i;

            PersonLinkControl plc = Links.Where(x => x.ID == pl.Key).FirstOrDefault();
            if(plc != null)
            {
                foreach (var link in Links)
                    link.UnHighlight();

                plc.Highlight();
            }
        }
        
        private void LinkedPersons_MouseRightButtonUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Delete)
                return;


            if (MessageBox.Show("Really delete Link?", "Please confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {

                var i = (sender as ListBox).SelectedItem;

                if (i == null)
                    return;

                KeyValuePair<Guid, string> pl = (KeyValuePair<Guid, string>)i;

                PersonLinkControl plc = Links.Where(x => x.ID == pl.Key).FirstOrDefault();
                if (plc != null)
                    L_OnDeleteClick(plc, EventArgs.Empty);
            }
        }
    }
}
