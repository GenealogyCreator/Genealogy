﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GenealogyCreator.Dialogs
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();

            Version version = Assembly.GetEntryAssembly().GetName().Version;
            AssemblyVersionLbl.Content = version.ToString();

            ReleaseNameLbl.Content = Properties.Settings.Default.app_releaseName 
                                    + " " + Properties.Settings.Default.app_version
                                    + ", " + Properties.Settings.Default.app_releaseDate;
        }

        private void CloseDialog_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
