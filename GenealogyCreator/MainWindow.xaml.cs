﻿//#define CanvasDragDebug
//#define TestPanels
using ColorHelper;
using GedNet;
using GenealogyCreator.Controls;
using GenealogyCreator.src;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Security.Cryptography;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Updater;

using SDColor = System.Drawing.Color;
using SWMColor = System.Windows.Media.Color;


namespace ColorHelper
{
    public static class ColorExt
    {
        public static SWMColor ToSWMColor(this SDColor color) => SWMColor.FromArgb(color.A, color.R, color.G, color.B);
        public static SDColor ToSDColor(this SWMColor color) => SDColor.FromArgb(color.A, color.R, color.G, color.B);
    }
}

//TODO: Use Settings-Bindings in GUI
//TODO: Use Logging-Framework


namespace GenealogyCreator
{

    public partial class MainWindow : Fluent.RibbonWindow
    {

        private Dictionary<Person, PersonPanel> panelDict;
        private Dictionary<PersonPanel, Thickness> selectedPanels;

        private Person selectedPerson;

        private Point dragPanelStart;
        private Thickness startMargin;

        private Timer autosaveTimer;
        private DBBackupHandler BackupHandler;
        private Logger logger = LogManager.GetCurrentClassLogger();

        private double zoomFactor = 1;
        private ResourceManager rm = new ResourceManager("GenealogyCreator.Properties.Resources", Assembly.GetExecutingAssembly());

        private GenealogyContext db;
        public MainWindow()
        {
            logger.Trace("Starting");

            InitializeComponent();

            logger.Trace("Starting GenealogyCreator v" + Assembly.GetEntryAssembly().GetName().Version.ToString());
            logger.Trace("Release: " + Properties.Settings.Default.app_releaseName
                                    + " " + Properties.Settings.Default.app_version
                                    + ", " + Properties.Settings.Default.app_releaseDate);


            panelDict = new Dictionary<Person, PersonPanel>();

            selectedPanels = new Dictionary<PersonPanel, Thickness>();

            if (Properties.Settings.Default.UpdateSettings)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpdateSettings = false;
                Properties.Settings.Default.Save();
                logger.Debug("UserSettings Updated.");
            }


            UpdateWorker updater = new UpdateWorker();
            updater.localDir = System.IO.Directory.GetCurrentDirectory();
            updater.remoteDir = Properties.Settings.Default.app_remoteDir;
            updater.writeVersion(
               Properties.Settings.Default.app_title,
               Properties.Settings.Default.app_releaseName,
               Properties.Settings.Default.app_version,
               Properties.Settings.Default.app_releaseDate,
               Assembly.GetEntryAssembly().GetName().Version.ToString());

            try
            {
                logger.Trace("Start Check for Updates!");
                bool? updateAvailable = updater.CheckForUpdate();
                if (updateAvailable.HasValue)
                {
                    if (updateAvailable == true)
                    {
                        try
                        {
                            if (MessageBox.Show(rm.GetString("UpdateFound_Install"), rm.GetString("PleaseConfirm"), MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                updater.PrepareUpdate();
                                updater.StartUpdaterProc();
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    logger.Warn("Unable to Check for Updates!");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fehler beim Prüfen auf Updates.");
                logger.Debug("Error checking Updates: " + e);
            }


            if (Properties.Settings.Default.db_connection_string == "DEFAULT")
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, "GenealogyCreator");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                Properties.Settings.Default.db_connection_string = "Data Source=" + Path.Combine(path, "Database.gdb") + ";";
            }


            if (Properties.Settings.Default.mediaDirectory == "DEFAULT")
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                path = Path.Combine(path, "GenealogyCreator");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                Properties.Settings.Default.mediaDirectory = path;
            }



            try
            {
                logger.Trace("Opening DB from " + Properties.Settings.Default.db_connection_string);
                db = new GenealogyContext(Properties.Settings.Default.db_connection_string);
            }
            catch (Exception e)
            {
                logger.Warn(e.Message);
                throw;
            }

            if (!db.Database.Exists())
            {
                logger.Warn("Database not found. Creating Database: " + Properties.Settings.Default.app_version.ToString());
                try
                {
                    db.Database.Create();
                    logger.Trace("Database created. ");
                }
                catch (Exception e)
                {
                    logger.Warn("Error creating Database: " + e);
                    throw;
                }
            }

            db.Configuration.LazyLoadingEnabled = false;


            autosaveTimer = new Timer();
            autosaveTimer.Interval = 1000 * 60 * 3;
            autosaveTimer.Elapsed += AutosaveTimer_Elapsed;
            autosaveTimer.Start();

            BackupHandler = new DBBackupHandler(Properties.Settings.Default.db_backup_interval);


            perGPNameEdit.Visibility = Visibility.Collapsed;


            #region LoadSettings
            rbn_PDF_BaseFontSize.Value = Properties.Settings.Default.PDF_FontSize;
            rbn_PDF_PageWidth.Value = Properties.Settings.Default.PDF_PageWidth;

            rbn_DistHorizontal.Value = Properties.Settings.Default.autoArrangeDistanceHorizontal;
            rbn_DistVertical.Value = Properties.Settings.Default.autoArrangeDistanceVertical;

            rbn_CGChildrenSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.ChildLineSelectedColor);
            rbn_CGChildrenUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.ChildLineUnselectedColor);

            rbn_CGSpousesSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.SpouseLineSelectedColor);
            rbn_CGSpousesUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.SpouseLineUnselectedColor);

            rbn_CGPanelBorderSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderSelectedColor);
            rbn_CGPanelBorderUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderUnselectedColor);



            rbn_CGMaleUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Unselected);
            rbn_CGMaleSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Selected);

            rbn_CGFemaleUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Unselected);
            rbn_CGFemaleSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Selected);

            LineTypeGallery.SelectedIndex = Properties.Settings.Default.LineType;

            zoomFactor = Properties.Settings.Default.zoomFactor;
            setZoom(zoomFactor);
            #endregion


            //if (dbNew.Database.Exists() && MessageBox.Show("Delete old DB?", "?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            //{
            //    dbNew.Database.Delete();
            //    MessageBox.Show("DB deleted");
            //}


            /*
            if(!System.IO.File.Exists("gc.db"))
            {
                if(MessageBox.Show("Database not Found!\nImport existing database?\nIf not, empty DB will be created.", "Database not found!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    OpenFileDialog d = new OpenFileDialog();
                    if(d.ShowDialog().Value)
                    {
                        string selectedFile = d.FileName;
                        MessageBox.Show(selectedFile);
                        System.IO.File.Copy(selectedFile, "gc.db");
                    }
                }
            }
            */

            // Initialize NHibernate
            // factory = CreateNewFactory();
            // dbSession = factory.OpenSession();

            #region OLDSTUFF

            //THIS IS FOR FIRST INIT, COPY DATA FROM OLD DB
            /*
            if (MessageBox.Show("Import old DB?", "?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {

                SQLite db = SQLite.getInstance("genealogy.db");
                Dictionary<int, Person> personList = new Dictionary<int, Person>();

                // Fetch PersonData
                DataView dv = db.exec("SELECT firstName, lastName, birth, death, placeOfBirth, infoText, p_mother, p_spouse, posX, posY, ID, isDead, isMale FROM person;");
                foreach (DataRowView row in dv)
                {
                    Person p = new Person()
                    {
                        FirstName = row["firstName"].ToString(),
                        BirthName = row["lastName"].ToString(),
                        Birth = Convert.ToDateTime(row["birth"].ToString()),
                        Death = Convert.ToDateTime(row["death"].ToString()),
                        Place = row["placeOfBirth"].ToString(),
                        InfoText = row["infoText"].ToString(),
                        PanelPosition = new Thickness(Convert.ToInt32(row["posX"].ToString()), Convert.ToInt32(row["posY"].ToString()), 0, 0),
                        isMale = row["isMale"].ToString() == "1" ? true : false,
                        isDead = row["isDead"].ToString() == "1" ? true : false
                    };

                    personList.Add(Convert.ToInt32(row["ID"].ToString()), p);

                    try
                    {

                        dbNew.Persons.Add(p);
                        dbNew.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        throw;
                    }
                }


                //Fetch Spouses & Parents
                dv = db.exec("SELECT firstName, lastName, birth, death, placeOfBirth, infoText, p_mother, p_spouse, posX, posY, ID, isDead, isMale FROM person;");
                foreach (DataRowView row in dv)
                {
                    Person p;

                    personList.TryGetValue(Convert.ToInt32(row["ID"].ToString()), out p);


                    if (row["p_mother"].ToString() != null && row["p_mother"].ToString().Length > 0)
                    {
                        Person mother;

                        if (personList.TryGetValue(Convert.ToInt32(row["p_mother"].ToString()), out mother))
                        {
                            p.Parent = mother;
                        }
                    }


                    if (row["p_spouse"].ToString() != null && row["p_spouse"].ToString().Length > 0)
                    {
                        Person mother;

                        if (personList.TryGetValue(Convert.ToInt32(row["p_spouse"].ToString()), out mother))
                        {
                            p.Spouse = mother;
                        }
                    }
                }


                dbNew.SaveChanges();



                dv = db.exec("SELECT * FROM files;");

                foreach (DataRowView row in dv)
                {
                    int fileID = Convert.ToInt32(row["ID"].ToString());
                    int personID = Convert.ToInt32(row["p_ID"].ToString());
                    string filename = row["fileName"].ToString();

                    Document d = new Document() { Filename = filename };
                    if (d.Persons == null)
                        d.Persons = new List<Person>();


                    Person p;

                    if (personList.TryGetValue(personID, out p))
                    {
                        if (p.Documents == null)
                            p.Documents = new List<Document>();

                        p.Documents.Add(d);
                        d.Persons.Add(p);
                    }

                    dbNew.Documents.Add(d);

                }


                dbNew.SaveChanges();
            }
            */
            #endregion
            //dbSession.Flush();

            // Note that we do not use the table name specified
            // in the mapping, but the class name, which is a nice
            // abstraction that comes with NHibernate

            // List all the entries' names



            logger.Trace("Start loading data.");
            loadData();
            logger.Trace(" loading data done.");



            db.SaveChanges();

            logger.Trace("### UP AND RUNNING ###");
        }


        private void AutosaveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            db.SaveChanges();
        }


        private void loadData()
        {
            //TODO: Check if this is neccessary afer lazyloading is disabled
            var tmpPersons = db.People.Include(X => X.Events).ToList<Person>();
            tmpPersons = db.People.Include(X => X.MediaObjects).ToList<Person>();
            tmpPersons = db.People.Include(X => X.Links).ToList<Person>();
            var FamilyList = db.Families.Include(X => X.Spouses).ToList<Family>();
            //var oL = db.Objects.Include(X => X.Links).ToList();

            if (tmpPersons.Count == 0)
                tmpPersons.Add(new Person());

            addPersonsToGUI(tmpPersons);
            drawFamilyLines(db.Families.ToList());
        }



        #region Zooming

        private void setZoom(double zF)
        {
            if (zoomSettings == null || PanelArea == null)
                return;

            zoomFactor = zF;
            zoomSlider.Value = zoomFactor;

            var scaler = PanelArea.LayoutTransform as ScaleTransform;
            zoomFactor = Math.Round(zoomSlider.Value, 4);
            zoomSettings.Content = rm.GetString("Zoom") + ": " + Math.Round(zF * 100) + "%";

            double scrollPosY_rel = (100 / PanelAreaScrollViewer.ScrollableHeight) * PanelAreaScrollViewer.VerticalOffset;
            double scrollPosX_rel = (100 / PanelAreaScrollViewer.ScrollableWidth) * PanelAreaScrollViewer.HorizontalOffset;

            if (scaler == null)
            {
                PanelArea.LayoutTransform = new ScaleTransform(1, 1);
            }
            else
            {
                scaler.ScaleX = zoomFactor;
                scaler.ScaleY = zoomFactor;
            }

            PanelAreaScrollViewer.UpdateLayout();
            try
            {
                if (PanelAreaScrollViewer.ScrollableHeight > 0 && !Double.IsNaN(scrollPosY_rel))
                {
                    double newScrollPosY = scrollPosY_rel / (100 / PanelAreaScrollViewer.ScrollableHeight);
                    PanelAreaScrollViewer.ScrollToVerticalOffset(newScrollPosY);
                }

                if (PanelAreaScrollViewer.ScrollableWidth > 0 && !Double.IsNaN(scrollPosX_rel))
                {
                    double newScrollPosX = scrollPosX_rel / (100 / PanelAreaScrollViewer.ScrollableWidth);
                    PanelAreaScrollViewer.ScrollToHorizontalOffset(newScrollPosX);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            Properties.Settings.Default.zoomFactor = zoomFactor;
            Properties.Settings.Default.Save();
        }


        private void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            setZoom(zoomSlider.Value);
        }

        private void zoomReset_Click(object sender, RoutedEventArgs e)
        {
            setZoom(1);
        }


        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                e.Handled = true;
                double d = e.Delta;
                zoomFactor = zoomFactor + (d / 1600.0);
                setZoom(zoomFactor);
            }
            else
            {
                e.Handled = false;
            }
        }

        #endregion



        private void addPersonsToGUI(List<Person> personsList)
        {
            //Create Panels for each Person
            foreach (Person p in personsList)
            {
                Controls.PersonPanel panel = new Controls.PersonPanel(p);

                panel.PreviewMouseLeftButtonDown += Panel_PreviewMouseLeftButtonDown;
                panel.MouseMove += Panel_MouseMove;
                panel.MouseLeftButtonUp += Panel_MouseLeftButtonUp;
                panel.PreviewKeyDown += PanelArea_PreviewKeyDown;
                panel.MouseDoubleClick += Panel_MouseDoubleClick;
                panelDict.Add(p, panel);
            }


            //Draw Panels here, so they are all on top
            foreach (var pPair in panelDict)
                try
                {
                    PanelArea.Children.Add(pPair.Value);
                    Canvas.SetZIndex(pPair.Value, 100);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
        }

        private void Panel_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            loadPersonsData(sender as PersonPanel);
            MainTabControl.SelectedIndex = 2;
        }

        private void drawFamilyLines(List<Family> familyList)
        {
            foreach (var f in familyList)
            {
                foreach (var parent in f.Spouses)
                {
                    foreach (var child in f.Children)
                    {
                        drawChildLine(parent, child);
                    }

                    foreach (var spouse in f.Spouses)
                    {
                        if (parent != spouse)
                            drawSpouseLine(parent, spouse);
                    }
                }
            }


        }


        #region PanelDragnDrop

        private void Panel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            PersonPanel pp = sender as PersonPanel;

            if (Keyboard.Modifiers != ModifierKeys.Control)
            {
                foreach (var pps in selectedPanels)
                    pps.Key.unselect();

                selectedPanels.Clear();

                selectedPanels.Add(pp, pp.Margin);
                dragPanelStart = e.GetPosition(null);
                pp.select();
                startMargin = pp.Margin;

                if ((bool)rbn_cb_ShowFamilyLine.IsChecked)
                {
                    Person starter = pp.getPerson();
                    highlightChildren(starter);
                    highlightParents(starter);

                    foreach (Family f in db.Families)
                        f.isHighlightet = false;
                }
            }
        }

        private void highlightFamilyline(Person starter)
        {
            foreach (Family f in starter.Spouses)
            {
                if (!f.isHighlightet)
                {
                    //highlightChildren(f, starter);
                    f.isHighlightet = true;
                }
            }

        }

        private void highlightParents(Person origin)
        {
            if (origin.IsChildOf != null)
            {
                foreach (Person p in origin.IsChildOf.Spouses)
                {
                    if (p != origin)
                    {
                        panelDict[p].select(Line.LineTarget.Parent);
                        if (!selectedPanels.ContainsKey(panelDict[p]))
                            selectedPanels.Add(panelDict[p], panelDict[p].Margin);
                        highlightParents(p);
                    }
                }
            }



        }

        private void highlightChildren(Person origin)
        {
            foreach (Family f in origin.Spouses)
            {
                if (!f.isHighlightet)
                {
                    foreach (Person p in f.Children)
                    {
                        if (p != origin)
                        {
                            panelDict[p].select(Line.LineTarget.Child);
                            if (!selectedPanels.ContainsKey(panelDict[p]))
                                selectedPanels.Add(panelDict[p], panelDict[p].Margin);
                            highlightChildren(p);
                        }
                    }
                }
            }
        }

        private void Panel_MouseMove(object sender, MouseEventArgs e)
        {
            // Get the current mouse position
            Point mousePos = e.GetPosition(null);
            Vector diff = dragPanelStart - mousePos;

            if (
                e.LeftButton == MouseButtonState.Pressed
                && (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance
                || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
                )
            {

                foreach (var pps in selectedPanels)
                    pps.Key.Offset(diff, zoomFactor, pps.Value);
            }
        }

        private void Panel_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PersonPanel pp = sender as PersonPanel;
            dragPanelStart = e.GetPosition(null);

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (!pp.Selected)
                {
                    selectedPanels.Add(pp, pp.Margin);
                    startMargin = pp.Margin;
                    pp.select();
                }
                else
                {
                    selectedPanels.Remove(pp);
                    pp.unselect();
                }
            }
            else
            {
                //foreach (var pps in selectedPanels)
                //    pps.unselect();

                //selectedPanels.Clear();

                //selectedPanels.Add(pp);
                //dragPanelStart = e.GetPosition(null);
                //pp.select();
            }

            if (e.ClickCount == 2)
                MainTabControl.SelectedIndex = 2;

            loadPersonsData(pp);

            e.Handled = true;
        }

        #endregion

        #region  MultiSelect
        private Point MultiSelectStart;
        private Point MultiSelectStop;

        private void PanelArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PanelArea.CaptureMouse();

            foreach (var pp in selectedPanels)
                pp.Key.unselect();
            selectedPanels.Clear();


            MultiSelectStart = e.GetPosition(PanelArea);

#if CanvasDragDebug
            Point pTMP = e.GetPosition(null);
            p1.Margin = new Thickness(pTMP.X, pTMP.Y, 0, 0);
#endif

            // Initial placement of the drag selection box.         
            Canvas.SetLeft(selectionBox, MultiSelectStart.X);
            Canvas.SetTop(selectionBox, MultiSelectStart.Y);
            selectionBox.Width = 0;
            selectionBox.Height = 0;

            // Make the drag selection box visible.
            selectionBox.Visibility = Visibility.Visible;
        }

        private void PanelArea_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (MultiSelectStart.X == 0 && MultiSelectStart.Y == 0)
                return;

            PanelArea.ReleaseMouseCapture();
            selectionBox.Visibility = Visibility.Collapsed;
            MultiSelectStop = e.GetPosition(PanelArea);

#if CanvasDragDebug
            Point pTMP = e.GetPosition(null);
            pTMP.Offset(PanelArea.Margin.Left, PanelArea.Margin.Top);
            p2.Margin = new Thickness(pTMP.X, pTMP.Y, 0, 0);
#endif

            Point topLeft = new Point();
            Point bottomRight = new Point();

            topLeft.X = MultiSelectStart.X < MultiSelectStop.X ? MultiSelectStart.X : MultiSelectStop.X;
            topLeft.Y = MultiSelectStart.Y < MultiSelectStop.Y ? MultiSelectStart.Y : MultiSelectStop.Y;

            bottomRight.X = MultiSelectStart.X > MultiSelectStop.X ? MultiSelectStart.X : MultiSelectStop.X;
            bottomRight.Y = MultiSelectStart.Y > MultiSelectStop.Y ? MultiSelectStart.Y : MultiSelectStop.Y;


            foreach (var pp in panelDict)
            {
                Point p = new Point();
                p.X = pp.Value.Margin.Left;
                p.Y = pp.Value.Margin.Top;

                if ((p.X >= topLeft.X && p.X <= bottomRight.X) && (p.Y >= topLeft.Y && p.Y <= bottomRight.Y))
                {
                    pp.Value.select();

                    if (!selectedPanels.ContainsKey(pp.Value))
                        selectedPanels.Add(pp.Value, pp.Value.Margin);
                }
            }

            MultiSelectStop.X = MultiSelectStop.Y = 0;
            MultiSelectStart.X = MultiSelectStart.Y = 0;

        }

        #endregion

        #region DragArea

        private Point areaDragStart;

        private void PanelArea_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            areaDragStart = e.GetPosition(PanelArea);
            e.Handled = true;
        }


        private void PanelArea_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed) //Scroll with Right Mouse
            {
                Point mousePos = e.GetPosition(null);
                Vector diff = areaDragStart - mousePos;

                if ((Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                 Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    PanelAreaScrollViewer.ScrollToHorizontalOffset(PanelAreaScrollViewer.HorizontalOffset - (diff.X / 2));
                    PanelAreaScrollViewer.ScrollToVerticalOffset(PanelAreaScrollViewer.VerticalOffset - (diff.Y / 2));
                }
            }
            else if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point mousePos = e.GetPosition(PanelArea);

                if (MultiSelectStart.X < mousePos.X)
                {
                    Canvas.SetLeft(selectionBox, MultiSelectStart.X);
                    selectionBox.Width = mousePos.X - MultiSelectStart.X;
                }
                else
                {
                    Canvas.SetLeft(selectionBox, mousePos.X);
                    selectionBox.Width = MultiSelectStart.X - mousePos.X;
                }

                if (MultiSelectStart.Y < mousePos.Y)
                {
                    Canvas.SetTop(selectionBox, MultiSelectStart.Y);
                    selectionBox.Height = mousePos.Y - MultiSelectStart.Y;
                }
                else
                {
                    Canvas.SetTop(selectionBox, mousePos.Y);
                    selectionBox.Height = MultiSelectStart.Y - mousePos.Y;
                }
            }
        }



        #endregion


        private void loadPersonsData(PersonPanel pp)
        {
            loadPersonsData(pp.getPerson());
        }

        private void loadPersonsData(Person p)
        {
            if (p == null)
                return;

            selectedPerson = p;

            //Load into Compact View
            loadCompactView(p);

            //
            loadPersonView(p);
        }

        private void loadPersonView(Person p)
        {
            perEventGrid.ItemsSource = null;
            perEventGrid.ItemsSource = p.Events;

            personDetailGrid.DataContext = null;
            personDetailGrid.DataContext = p;

            perParentList.Items.Clear();
            perChildrenList.Items.Clear();
            perSpouseList.Items.Clear();

            List<Person> Parents = p.getParents().ToList<Person>();
            foreach (var prnt in Parents)
            {
                ExtendedPersonPanel ep = new ExtendedPersonPanel(prnt);
                ep.MouseDoubleClick += Ep_MouseDoubleClick;
                perParentList.Items.Add(ep);
            }

            List<Person> Children = p.getChildren().ToList<Person>();
            foreach (var prnt in Children)
            {
                ExtendedPersonPanel ep = new ExtendedPersonPanel(prnt);
                ep.MouseDoubleClick += Ep_MouseDoubleClick;
                perChildrenList.Items.Add(ep);
            }

            List<Person> Spouses = p.getSpouses().ToList<Person>();
            foreach (var prnt in Spouses)
            {
                ExtendedPersonPanel ep = new ExtendedPersonPanel(prnt);
                ep.MouseDoubleClick += Ep_MouseDoubleClick;
                perSpouseList.Items.Add(ep);
            }
        }

        private void Ep_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExtendedPersonPanel ep = sender as ExtendedPersonPanel;
            loadPersonsData(ep.itsPerson);
        }

        private void loadCompactView(Person p)
        {
            epPerson.Load(p);

            cmpctChildrensList.Items.Clear();

            foreach (var fam in p.Spouses)
                foreach (var c in fam.Children)
                    cmpctChildrensList.Items.Add(c);

            loadParentsPanel(p, epParents);

            epHusbandParents.Clear();
            epWifeParents.Clear();
            epHusbandHusbandParents.Clear();
            epHusbandWifeParents.Clear();
            epWifeHusbandParents.Clear();
            epWifeWifeParents.Clear();

            loadParentsPanel(epParents.HusbandPanel.itsPerson, epHusbandParents);
            loadParentsPanel(epParents.WifePanel.itsPerson, epWifeParents);


            loadParentsPanel(epHusbandParents.HusbandPanel.itsPerson, epHusbandHusbandParents);
            loadParentsPanel(epHusbandParents.WifePanel.itsPerson, epHusbandWifeParents);


            loadParentsPanel(epWifeParents.HusbandPanel.itsPerson, epWifeHusbandParents);
            loadParentsPanel(epWifeParents.WifePanel.itsPerson, epWifeWifeParents);


            //if (!epParents.HusbandPanel.IsEmpty)
            //    loadParentsPanel(epParents.HusbandPanel.itsPerson, epHusbandParents);
            //if (!epParents.WifePanel.IsEmpty)
            //    loadParentsPanel(epParents.WifePanel.itsPerson, epWifeParents);


            //if (!epHusbandParents.HusbandPanel.IsEmpty)
            //    loadParentsPanel(epHusbandParents.HusbandPanel.itsPerson, epHusbandHusbandParents);
            //if (!epHusbandParents.WifePanel.IsEmpty)
            //    loadParentsPanel(epHusbandParents.WifePanel.itsPerson, epHusbandWifeParents);


            //if (!epWifeParents.HusbandPanel.IsEmpty)
            //    loadParentsPanel(epWifeParents.HusbandPanel.itsPerson, epWifeHusbandParents);
            //if (!epWifeParents.WifePanel.IsEmpty)
            //    loadParentsPanel(epWifeParents.WifePanel.itsPerson, epWifeWifeParents);

        }

        private void loadParentsPanel(Person p, SpousePanel epParents)
        {
            epParents.Clear();
            epParents.refPerson = p;

            if (p == null || p.IsChildOf == null)
                return;

            if (p.IsChildOf.Spouses.Count <= 0)
                return;


            Family f = p.IsChildOf;

            Person A = f.Spouses.ElementAt(0);

            epParents.FamilyLink.Content = f.IDf.ToString().Substring(0, 8).ToUpper();

            if (A.isMale)
                epParents.HusbandPanel.Load(A);
            else
                epParents.WifePanel.Load(A);

            if (f.Spouses.Count >= 2)
            {
                Person B = f.Spouses.ElementAt(1);

                if (B.isMale)
                {
                    if (!epParents.HusbandPanel.IsEmpty)
                    {
                        MessageBox.Show(rm.GetString("HusbandPanelAlreadyFilled"));
                    }
                    else
                        epParents.HusbandPanel.Load(B);
                }
                else
                {
                    if (!epParents.WifePanel.IsEmpty)
                    {
                        MessageBox.Show(rm.GetString("WifePanelAlreadyFilled"));
                    }
                    else
                        epParents.WifePanel.Load(B);

                }

            }

        }

        private void btn_ResetPerson_Click(object sender, RoutedEventArgs e)
        {
            //loadPersonsData();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            db.SaveChanges();
        }

        private void lb_Files_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = sender as ListBox;

            if (lb.SelectedIndex < 0)
                return;
        }

        private void rbnAddPerson_Click(object sender, RoutedEventArgs e)
        {


            Person p = new Person();


            Controls.PersonPanel panel = new Controls.PersonPanel(p);
            db.People.Add(p);

            panel.PreviewMouseLeftButtonDown += Panel_PreviewMouseLeftButtonDown;
            panel.MouseMove += Panel_MouseMove;
            panel.MouseLeftButtonUp += Panel_MouseLeftButtonUp;
            panelDict.Add(p, panel);
            PanelArea.Children.Add(panel);

        }

        private void rbn_ExportDB_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Neccesary feature?
        }

        private void rbn_ImportDB_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, Person> personDictTMP = new Dictionary<string, Person>();
            List<Person> personListTMP = new List<Person>();
            OpenFileDialog fb = new OpenFileDialog();
            fb.Filter = "GedCom " + rm.GetString("Documents") + "|*.ged";

            bool a = (bool)fb.ShowDialog();
            if (a == true)
            {
                Gedcom gc = new Gedcom();
                gc.fromFile(fb.FileName);
                foreach (INDI i in gc.Individuals.Values)
                {
                    Person p = new Person(i);

                    db.People.Add(p);
                    personListTMP.Add(p);
                    personDictTMP.Add(p.gedcomID, p);
                }


                foreach (FAM i in gc.Families.Values)
                {
                    Family f = new Family();

                    foreach (INDI s in i.Spouses)
                    {
                        Person p = personDictTMP[s.gedcomID];
                        p.Spouses.Add(f);
                        f.Spouses.Add(p);
                    }

                    foreach (INDI s in i.Children)
                    {
                        Person p = personDictTMP[s.gedcomID];
                        //p.isChildOf =f;
                        p.IsChildOfID = f.IDf;
                        f.Children.Add(p);

                    }

                    foreach (var s in i.Events)
                        f.Events.Add(new EVENT(s));


                    db.Families.Add(f);
                }
            }


            addPersonsToGUI(personListTMP);
            drawFamilyLines(db.Families.ToList());


            db.SaveChanges();
        }

        private void drawSpouseLine(Person parent, Person spouse)
        {
            drawLine(parent, spouse, true);
        }

        private void drawChildLine(Person parent, Person child)
        {
            drawLine(parent, child, false);
        }

        private void drawLine(Person persA, Person persB, bool isSpouseLine)
        {
            bool tryGetA;
            bool tryGetB;

            PersonPanel pPanelA;
            PersonPanel pPanelB;

            tryGetA = panelDict.TryGetValue(persA, out pPanelA);
            tryGetB = panelDict.TryGetValue(persB, out pPanelB);

            if (tryGetA && tryGetB)
            {
                if (!pPanelA.LineExists(persB.ID))
                {
                    Line l = new Line(ref pPanelA, ref pPanelB, isSpouseLine, persA, persB, (Line.LineType)Properties.Settings.Default.LineType);


                    foreach (UIElement uiLine in l.getLines())
                    {
                        PanelArea.Children.Add(uiLine);
                        Canvas.SetZIndex(uiLine, 10);
                    }

                    if (isSpouseLine)
                    {
                        pPanelA.addLine(ref l, Line.LineTarget.Spouse);
                        pPanelB.addLine(ref l, Line.LineTarget.Spouse);
                    }
                    else
                    {
                        pPanelA.addLine(ref l, Line.LineTarget.Child);
                        pPanelB.addLine(ref l, Line.LineTarget.Parent);
                    }
                }
            }
        }

        private void rbn_CreatePDF_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF " + rm.GetString("Documents") + "(.pdf)|*.pdf"; // Filter files by extension
            sfd.FileName = "Genealogy"; // Default file name
            sfd.DefaultExt = ".pdf"; // Default file extension

            // Show save file dialog box
            Nullable<bool> result = sfd.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = sfd.FileName;
                PDFCreator p = new PDFCreator(filename);
                p.createPDF();
            }

        }


        private void rbn_DistVertical_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.autoArrangeDistanceVertical = (int)rbn_DistVertical.Value;
            Properties.Settings.Default.Save();

        }

        private void rbn_DistHorizontal_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.autoArrangeDistanceHorizontal = (int)rbn_DistHorizontal.Value;
            Properties.Settings.Default.Save();
        }

        private void rbn_CGPanelBorderSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBorderSelectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGPanelBorderSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGPanelBorderUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBorderUnselectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGPanelBorderUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGChildrenSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ChildLineSelectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGChildrenSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGChildrenUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ChildLineUnselectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGChildrenUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGSpousesSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.SpouseLineSelectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGSpousesSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGMaleUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudMale_Unselected = ColorExt.ToSDColor((SWMColor)rbn_CGMaleUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGFemaleUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudFemale_Unselected = ColorExt.ToSDColor((SWMColor)rbn_CGFemaleUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGMaleSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudMale_Selected = ColorExt.ToSDColor((SWMColor)rbn_CGMaleSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGFemaleSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudFemale_Selected = ColorExt.ToSDColor((SWMColor)rbn_CGFemaleSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }





        private void rbn_CGSpousesUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.SpouseLineUnselectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGSpousesUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_PDF_PageWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.PDF_PageWidth = (int)rbn_PDF_PageWidth.Value;
            Properties.Settings.Default.Save();
        }

        private void rbn_PDF_BaseFontSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.PDF_FontSize = (int)rbn_PDF_BaseFontSize.Value;
            Properties.Settings.Default.Save();
        }

        private void lineGal_Straight_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Straight);
        }

        private void lineGal_RectA_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Rect_A);
        }

        private void lineGal_RectB_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Rect_B);
        }

        private void lineGal_AngleA_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Angular_A);
        }

        private void saveLineType(Line.LineType t)
        {
            Properties.Settings.Default.LineType = (int)t;
            Properties.Settings.Default.Save();
            LineTypeGallery.SelectedIndex = Properties.Settings.Default.LineType;
            foreach (PersonPanel p in panelDict.Values)
                p.UpdateLine();

            //Delete all Lines
            for (int i = PanelArea.Children.Count - 1; i >= 0; --i)
            {
                UIElement item = PanelArea.Children[i];
                if (item.GetType() == typeof(System.Windows.Shapes.Line))
                {
                    PanelArea.Children.Remove(item);
                }
            }


            foreach (PersonPanel p in panelDict.Values)
                foreach (Line l in p.Lines.Keys)
                    foreach (UIElement uiLine in l.getLines())
                    {
                        try
                        {
                            if (!PanelArea.Children.Contains(uiLine))
                            {
                                PanelArea.Children.Add(uiLine);
                                Canvas.SetZIndex(uiLine, 10);
                            }

                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                    }
        }


        #region Arrangement

        private Dictionary<int, int> RowInCol;
        private int generationWidth;
        private int generationHeight;

        private void rbn_OrderAutomatic_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(rm.GetString("MoveAndOrderAllPeopleAutomatically"), rm.GetString("PleaseConfirm"), MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            generationWidth = 180 + Properties.Settings.Default.autoArrangeDistanceHorizontal;
            generationHeight = 60 + Properties.Settings.Default.autoArrangeDistanceVertical;

            RowInCol = new Dictionary<int, int>();
            int generationColumn = 15;

            foreach (Person pTmp in db.People)
                pTmp.isArranged = false;


            Person p = db.People.First();
            p.isArranged = true;
            PersonPanel panel = panelDict[p];

            if (!RowInCol.ContainsKey(generationColumn))
                RowInCol.Add(generationColumn, 0);

            int RowPos = RowInCol[generationColumn];
            RowPos += generationHeight;

            panel.updatePosition(new Thickness(generationColumn * generationWidth, RowPos, 0, 0));

            RowInCol[generationColumn] = RowPos;

            //Recursive call
            arrangePersonsFamily(p, generationColumn);
        }

        private void arrangePersonsFamily(Person p, int generationColumn)
        {
            Family parentsFamily = p.IsChildOf;
            if (parentsFamily != null)
            {
                arrangePanelsToColumn(generationColumn, parentsFamily.Children.ToList<Person>(), p);
                generationColumn -= 1;
                arrangePanelsToColumn(generationColumn, parentsFamily.Spouses.ToList<Person>(), p);

                generationColumn += 1;
            }


            foreach (Family f in p.Spouses)
            {
                arrangePanelsToColumn(generationColumn, f.Spouses.ToList<Person>(), p);
                arrangePanelsToColumn(generationColumn + 1, f.Children.ToList<Person>(), p);
            }
        }

        private void arrangePanelsToColumn(int generationColumn, List<Person> PersonList, Person pExclude = null)
        {
            foreach (Person p in PersonList)
            {
                if ((pExclude != null && p == pExclude) || p.isArranged)
                    continue;

                PersonPanel panel = panelDict[p];


                if (!RowInCol.ContainsKey(generationColumn))
                    RowInCol.Add(generationColumn, 0);

                int RowPos = RowInCol[generationColumn];
                RowPos += generationHeight;
                panel.updatePosition(new Thickness((generationColumn) * generationWidth, RowPos, 0, 0));

                RowInCol[generationColumn] = RowPos;
                p.isArranged = true;


                arrangePersonsFamily(p, generationColumn);
            }
        }

        private void rbn_spreadVertical_Click(object sender, RoutedEventArgs e)
        {
            spreadPanelsVertical();
        }

        private void rbn_spreadHorizontal_Click(object sender, RoutedEventArgs e)
        {
            spreadPanelsHorizontal();
        }

        private void rbn_alignTop_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsTop();
        }

        private void rbn_alignBottom_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsBottom();
        }

        private void rbn_alignLeft_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsLeft();
        }

        private void rbn_alignRight_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsRight();
        }



        private void alignPanelsRight()
        {
            double maxX = -1;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Left > maxX)
                    maxX = pp.Margin.Left;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(maxX, pp.Margin.Top, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void alignPanelsLeft()
        {
            double minX = Double.MaxValue;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Left < minX)
                    minX = pp.Margin.Left;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(minX, pp.Margin.Top, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void alignPanelsTop()
        {
            double minY = Double.MaxValue;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Top < minY)
                    minY = pp.Margin.Top;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(pp.Margin.Left, minY, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void alignPanelsBottom()
        {
            double maxY = -1;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Top > maxY)
                    maxY = pp.Margin.Top;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(pp.Margin.Left, maxY, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void spreadPanelsVertical()
        {
            double minY = Double.MaxValue, maxY = 0;
            double k;
            int cnt = 0;
            Dictionary<double, PersonPanel> tmpDict = new Dictionary<double, PersonPanel>();

            foreach (PersonPanel pp in selectedPanels.Keys)
            {
                if (pp.Margin.Top < minY)
                    minY = pp.Margin.Top;

                if (pp.Margin.Top > maxY)
                    maxY = pp.Margin.Top;

                k = pp.Margin.Top;
                while (tmpDict.ContainsKey(k))
                    k++;

                tmpDict.Add(k, pp);
            }

            IOrderedEnumerable<KeyValuePair<double, PersonPanel>> orderedList = tmpDict.OrderBy(key => key.Key);

            double distanceTotal = maxY - minY;

            double distanceBetween = distanceTotal / (selectedPanels.Count - 1);

            foreach (var ptemp in orderedList)
            {
                PersonPanel pp = ptemp.Value;
                pp.updatePosition(new Thickness(pp.Margin.Left, minY + distanceBetween * cnt, 0, 0));
                selectedPanels[pp] = pp.Margin;
                ++cnt;
            }
        }

        private void spreadPanelsHorizontal()
        {
            double minX = Double.MaxValue, maxX = 0;
            double k;
            int cnt = 0;
            Dictionary<double, PersonPanel> tmpDict = new Dictionary<double, PersonPanel>();

            foreach (PersonPanel pp in selectedPanels.Keys)
            {
                if (pp.Margin.Left < minX)
                    minX = pp.Margin.Left;

                if (pp.Margin.Left > maxX)
                    maxX = pp.Margin.Left;

                k = pp.Margin.Left;
                while (tmpDict.ContainsKey(k))
                    k++;

                tmpDict.Add(k, pp);
            }

            IOrderedEnumerable<KeyValuePair<double, PersonPanel>> orderedList = tmpDict.OrderBy(key => key.Key);

            double distanceTotal = maxX - minX;

            double distanceBetween = distanceTotal / (selectedPanels.Count - 1);

            foreach (var ptemp in orderedList)
            {
                PersonPanel pp = ptemp.Value;
                pp.updatePosition(new Thickness(minX + distanceBetween * cnt, pp.Margin.Top, 0, 0));
                selectedPanels[pp] = pp.Margin;
                ++cnt;
            }
        }


        #endregion

        private void PanelArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (selectedPanels.Count > 0 && ((e.Key == Key.Down) || (e.Key == Key.Up) || (e.Key == Key.Left) || (e.Key == Key.Right)))
            {
                int delta = 5;

                if (Keyboard.Modifiers == ModifierKeys.Control)
                    delta *= 15;

                for (int i = 0; i < selectedPanels.Count; ++i)
                {
                    PersonPanel pp = selectedPanels.ElementAt(i).Key;
                    if (e.Key == Key.Down)
                        pp.updatePosition(new Thickness(pp.Margin.Left, pp.Margin.Top + delta, 0, 0));
                    else if (e.Key == Key.Up)
                        pp.updatePosition(new Thickness(pp.Margin.Left, pp.Margin.Top - delta, 0, 0));
                    else if (e.Key == Key.Left)
                        pp.updatePosition(new Thickness(pp.Margin.Left - delta, pp.Margin.Top, 0, 0));
                    else if (e.Key == Key.Right)
                        pp.updatePosition(new Thickness(pp.Margin.Left + delta, pp.Margin.Top, 0, 0));

                    selectedPanels[pp] = pp.Margin;
                }

                e.Handled = true;
            }
        }

        private void rbn_File_Save_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
        }

        private void rbn_File_Exit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RibbonWindow_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TabItem currentMainTab = MainTabControl.SelectedItem as TabItem;

            if (currentMainTab.Name == "CompleteTreeTab")
            {
                if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.S)
                    db.SaveChanges();
                else if (e.Key == Key.NumPad4)
                    alignPanelsLeft();
                else if (e.Key == Key.NumPad8)
                    alignPanelsTop();
                else if (e.Key == Key.NumPad6)
                    alignPanelsRight();
                else if (e.Key == Key.NumPad2)
                    alignPanelsBottom();
                else if (e.Key == Key.NumPad1)
                    spreadPanelsVertical();
                else if (e.Key == Key.NumPad9)
                    spreadPanelsHorizontal();
            }
        }


        private void epParents_PersonDoubleClick(object sender, Person e)
        {
            loadPersonsData(e);
        }

        private void cmpctChildrensList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = sender as ListBox;

            if (lb.SelectedItems.Count <= 0)
                return;

            Person p = lb.SelectedItem as Person;
            loadPersonsData(p);

        }


        private void SpousePanel_OnAddPersonClick(object sender, AddPersonEventArgs e)
        {

            if (e.refPerson == null)
                return;

            if (MessageBox.Show(rm.GetString("WantToCreateAPerson"), rm.GetString("PleaseConfirm"), MessageBoxButton.YesNo) == MessageBoxResult.No)
                return;

            //Sometimes, there are Multiple Families.
            if ((e.relation == Family.Relationships.Daughter ||
                e.relation == Family.Relationships.Son ||
                e.relation == Family.Relationships.Children) &&
                e.refPerson.Spouses.Count > 1)
            {
                //TODO: is this in use?
                logger.Error("No family selected!");
                throw new Exception("No family selected!");

            }

            Person newPerson = new Person(e.refPerson, e.relation);

            db.People.Add(newPerson);


            if ((newPerson.IsChildOf != null))
            {
                //TODO: Why doesn't "db.Families.Where(i =" work?
                var cnts = db.Families.ToList();
                var cn = cnts.Where(i => i.IDf == newPerson.IsChildOf.IDf).Count();
                if (cn <= 0)
                {
                    db.Families.Add(newPerson.IsChildOf);
                }
            }

            //TODO: Does this even work?
            foreach (var fam in newPerson.Spouses)
                if (db.Families.Where(f => f.IDf == fam.IDf).Count() <= 0)
                    db.Families.Add(fam);


            loadPersonsData(newPerson);

            Controls.PersonPanel panel = new Controls.PersonPanel(newPerson);
            panel.PreviewMouseLeftButtonDown += Panel_PreviewMouseLeftButtonDown;
            panel.MouseMove += Panel_MouseMove;
            panel.MouseLeftButtonUp += Panel_MouseLeftButtonUp;
            panel.PreviewKeyDown += PanelArea_PreviewKeyDown;
            panelDict.Add(newPerson, panel);


            PanelArea.Children.Add(panel);
            Canvas.SetZIndex(panel, 100);

            db.TrySave();

            drawFamilyLines(db.Families.ToList());

        }

        private void perName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            perGPNameEdit.Visibility = Visibility.Visible;

        }

        private void perBtnSave_Click(object sender, RoutedEventArgs e)
        {
            perGPNameEdit.Visibility = Visibility.Collapsed;
        }

        private void genderString_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            selectedPerson.isMale = !selectedPerson.isMale;
            personDetailGrid.DataContext = null;
            personDetailGrid.DataContext = selectedPerson;
        }

        private void btnPerAddPerson_Click(object sender, RoutedEventArgs e)
        {
            Family.Relationships r;
            TabItem currentMainTab = TcPerRelatives.SelectedItem as TabItem;

            switch (currentMainTab.Name)
            {
                case "TabChildren":
                    r = Family.Relationships.Children;
                    break;
                case "TabSpouses":
                    r = Family.Relationships.Spouse;
                    break;
                case "TabParents":
                    r = Family.Relationships.Parent;
                    break;
                default:
                    return;
            }

            SpousePanel_OnAddPersonClick(this, new AddPersonEventArgs(selectedPerson, r));
        }

        private void rbn_OpenDatabase_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Multiselect = false;
            ofd.ShowDialog();
            ofd.Filter = rm.GetString("GenealogyDatabase") + " (.gdb)|*.gdb"; // Filter files by extension

            if (ofd.FileName != null && ofd.FileName.Length > 0)
            {
                clearData();
                string connectionString = "Data Source=" + ofd.FileName + ";";
                db = new GenealogyContext(connectionString);

                Properties.Settings.Default.db_connection_string = connectionString;
                Properties.Settings.Default.Save();

                loadData();
            }
        }

        private void clearData()
        {
            autosaveTimer.Stop();

            PanelArea.Children.Clear();

            panelDict = new Dictionary<Person, PersonPanel>();
            selectedPanels = new Dictionary<PersonPanel, Thickness>();

        }

        private void rbn_NewDatabase_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.DefaultExt = ".gdb";
            sfd.Filter = rm.GetString("GenealogyDatabase") + " (.gdb)|*.gdb"; // Filter files by extension


            Nullable<bool> result = sfd.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = sfd.FileName;
                if (File.Exists(filename))
                {
                    MessageBox.Show(rm.GetString("FileAlreadyExists"));
                    return;
                }
                else
                {
                    clearData();

                    string connectionString = "Data Source=" + sfd.FileName + ";";
                    db = new GenealogyContext(connectionString);
                    db.Database.CreateIfNotExists();

                    Properties.Settings.Default.db_connection_string = connectionString;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void rbn_DB_Interval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (BackupHandler != null)
                BackupHandler.updateInterval();
        }

        private void rbn_File_Settings_Click(object sender, RoutedEventArgs e)
        {
            Dialogs.SettingsDialog settingsDialog = new Dialogs.SettingsDialog();
            settingsDialog.ShowDialog();
            if (settingsDialog.dbChanged)
            {
                clearData();
                db = new GenealogyContext(Properties.Settings.Default.db_connection_string);
                loadData();
            }
        }

        private void perBtnAddFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Multiselect = true;
            ofd.ShowDialog();

            if (ofd.FileNames.Length > 0)
            {
                Person p = selectedPerson;
                string targetDir = Properties.Settings.Default.mediaDirectory;
                foreach (string file in ofd.FileNames)
                {
                    string fileExt = Path.GetExtension(file);
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    string fileNameNew = fileName + DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss") + fileExt;
                    string hash;
                    try
                    {
                        using (var md5 = MD5.Create())
                        {
                            using (var stream = File.OpenRead(file))
                            {
                                var hashByte = md5.ComputeHash(stream);
                                hash = BitConverter.ToString(hashByte).Replace("-", "").ToLowerInvariant();
                            }
                        }

                        MediaObject o = new MediaObject();
                        o.FILE = fileNameNew;
                        o.TITL = fileName;
                        o.FORM = fileExt;

                        File.Copy(file, Path.Combine(targetDir, fileNameNew));
                        p.MediaObjects.Add(o);
                    }
                    catch
                    {

                    }
                }
            }
        }

        private void perObjects_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid g = sender as DataGrid;

            if (g.SelectedItem != null)
            {
                MediaObject o = g.SelectedItem as MediaObject;

                string FilePath = Path.Combine(Properties.Settings.Default.mediaDirectory, o.FILE);
                if (File.Exists(FilePath))
                {
                    if (o.FORM.ToLower() == ".png" || o.FORM.ToLower() == ".jpg")
                    {
                        Dialogs.FileViewer fv = new Dialogs.FileViewer(o, selectedPerson, ref db);
                        fv.Show();
                        db.SaveChanges();
                        loadPersonsData(selectedPerson);
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(FilePath);
                    }
                }
            }
        }



        private void perLinks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid g = sender as DataGrid;

            if (g.SelectedItem != null)
            {
                PersonLink pl = g.SelectedItem as PersonLink;
                MediaObject o = pl.Image;

                if (o == null)
                    return;

                string FilePath = Path.Combine(Properties.Settings.Default.mediaDirectory, o.FILE);
                if (File.Exists(FilePath))
                {
                    if (o.FORM.ToLower() == ".png" || o.FORM.ToLower() == ".jpg")
                    {
                        Dialogs.FileViewer fv = new Dialogs.FileViewer(o, selectedPerson, ref db);
                        fv.Show();
                        db.SaveChanges();

                        loadPersonsData(selectedPerson);
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(FilePath);
                    }
                }
            }
        }

        private void rbn_File_About_Click(object sender, RoutedEventArgs e)
        {
            Dialogs.About aboutDialog = new Dialogs.About();
            aboutDialog.ShowDialog();
        }

        private void RibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void tbPersonSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = tbPersonSearch.Text;
            lbPeopleSearchResult.ItemsSource = db.People.Where(p => (p.FirstName.ToLower().Contains(searchText.ToLower())) ||
                                                                    (p.LastName.ToLower().Contains(searchText.ToLower()))).ToList();
        }

        private void lbPeopleSearchResult_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox o = sender as ListBox;
            if (o.SelectedItems.Count < 1)
                return;

            var selectedItem = o.SelectedItem as Person;
            PersonPanel pp;
            panelDict.TryGetValue(selectedItem, out pp);

            if(pp != null)
            {
                scrollToPanel(pp);
                pp.select();
                loadPersonsData(pp);
            }
        }

        private void scrollToPanel(PersonPanel pp)
        {
            Coordinates p = new Coordinates(pp.Margin.Left, pp.Margin.Top);
            p.Scale(zoomFactor);

            if (PanelAreaScrollViewer.ScrollableHeight > 0 )
            {
                double targetPos = p.Height;
                PanelAreaScrollViewer.ScrollToVerticalOffset(targetPos - (PanelAreaScrollViewer.ActualHeight / 2));
            }

            if (PanelAreaScrollViewer.ScrollableWidth > 0 )
            {
                double targetPos =  p.Width;
                PanelAreaScrollViewer.ScrollToHorizontalOffset(targetPos - (PanelAreaScrollViewer.ActualWidth / 2));
            }
        }
        #region ResizeScrollSearchControl
        enum SizeDir
        {
            SizeNESW,
            SizeWE,
            SizeNS,
            NoSizeing
        }
        private SizeDir currentSizeDir;
        private Point startSizingPoint, currentSizingPoint;
        private Size startSize;

        private void GridZoomeElements_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startSizingPoint = e.GetPosition(MainTreeGrid);
            startSize = GridZoomeElements.RenderSize;
        }

        private void GridZoomeElements_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
        }

        private void GridZoomeElements_MouseMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(GridZoomeElements);
            if (e.LeftButton == MouseButtonState.Released)
            {
                if (mousePos.X < 3 && mousePos.Y > (GridZoomeElements.ActualHeight - 5))
                {
                    Mouse.OverrideCursor = Cursors.SizeNESW;
                    currentSizeDir = SizeDir.SizeNESW;
                }
                else if (mousePos.X < 3)
                {
                    Mouse.OverrideCursor = Cursors.SizeWE;
                    currentSizeDir = SizeDir.SizeWE;
                }
                else if (mousePos.Y > (GridZoomeElements.ActualHeight - 5))
                {
                    Mouse.OverrideCursor = Cursors.SizeNS;
                    currentSizeDir = SizeDir.SizeNS;
                }
                else
                {
                    Mouse.OverrideCursor = Cursors.Arrow;
                    currentSizeDir = SizeDir.NoSizeing;
                }
            }
            //Mouse Down:
            else
            {
                currentSizingPoint = e.GetPosition(MainTreeGrid);

                double sX = startSizingPoint.X - currentSizingPoint.X;
                double sY = startSizingPoint.Y - currentSizingPoint.Y;


                switch (currentSizeDir)
                {
                    case SizeDir.SizeNESW:
                        GridZoomeElements.Height = startSize.Height - sY;
                        GridZoomeElements.Width = startSize.Width + sX;
                        break;
                    case SizeDir.SizeWE:
                        GridZoomeElements.Width = startSize.Width + sX;
                        break;
                    case SizeDir.SizeNS:
                        GridZoomeElements.Height = startSize.Height  -sY;
                        break;
                    case SizeDir.NoSizeing:
                        break;
                    default:
                        break;
                }
            }
        }

        private void GridZoomeElements_MouseLeave(object sender, MouseEventArgs e)
        {
            //Reset Cursor
            Mouse.OverrideCursor = Cursors.Arrow;
            currentSizeDir = SizeDir.NoSizeing;
        }

        #endregion
    }
}
