﻿using GenealogyCreator.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.src
{
    public class MediaObject
    {
        [Key]
        public Guid IDmo { get; set; }

        public string FORM { get; set; }
        public string TITL { get; set; }
        public string FILE { get; set; }
        public string TYPE { get; set; }
        public string DESC { get; set; }
        
        public ICollection<PersonLink> Links { get; set; }

        public MediaObject()
        {
            IDmo = Guid.NewGuid();
            Links = new List<PersonLink>();

        }

        public override string ToString()
        {
            return TITL + ", " + FILE + TYPE;
        }
    }
}
