﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GenealogyCreator.src
{
    public class DBBackupHandler : IDisposable
    {
        private Timer BackupTimer;
        private Logger logger = LogManager.GetCurrentClassLogger();

        public DBBackupHandler(int BackupInterval)
        {
            BackupTimer = new Timer();
            BackupTimer.Elapsed += BackupTimer_Elapsed;
            updateInterval();
            BackupTimer.Start();
            logger.Trace("DBBackupHandler created.");
        }

        public DBBackupHandler()
        {
        }

        private void BackupTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            createBackup();
            removeBackups();
        }

        private void removeBackups()
        {
            int BackupAmount = Properties.Settings.Default.db_backup_amount;
            string BackupPath = Properties.Settings.Default.db_backup_path;
            string Filename = Path.Combine(BackupPath, getCurrentDbFile());

            List<string> BackupList = Directory.GetFiles(BackupPath).Where(g => g.StartsWith(Filename)).ToList();
            
            string toDelete;
            while (BackupList.Count > BackupAmount)
            {
                toDelete = BackupList.OrderBy(q => q).First();
                File.Delete(Path.Combine(BackupPath, toDelete));
                logger.Debug("Deleted Backup: " + toDelete);
            }

        }

        public void updateInterval()
        {
            BackupTimer.Stop();
            BackupTimer.Interval = 1000 * 60 * Properties.Settings.Default.db_backup_interval;
            BackupTimer.Start();
        }

        public string getCurrentDbFile()
        {

            string Filename = Properties.Settings.Default.db_connection_string;
            if (Filename.StartsWith("Data Source="))
                Filename = Filename.Substring(Filename.IndexOf("=") + 1);

            if (Filename.EndsWith(";"))
                Filename = Filename.Substring(0, Filename.Length - 1);


            return Filename;
        }

        private void createBackup()
        {
           string Filename = getCurrentDbFile();
           string BackupPath = Properties.Settings.Default.db_backup_path;


            string BackupName = Filename + DateTime.Now.ToString("_yyyy-MM-dd_HH-mm-ss") + ".gdb";

            try
            {
                if (!Directory.Exists(BackupPath))
                    Directory.CreateDirectory(BackupPath);

                File.Copy(Filename, Path.Combine(BackupPath, BackupName));
                logger.Info("Created Backup: " + Path.Combine(BackupPath, BackupName));
            }
            catch (Exception ex)
            {
                logger.Warn("Error creating Backup: " + ex.Message + ex.ToString());
                throw;
            }

        }

        public void Dispose()
        {
            if(BackupTimer!= null)
                BackupTimer.Dispose(); 
        }
    }
}
