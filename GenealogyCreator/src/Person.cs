﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows;
using GedNet;
using System.Windows.Data;
using System.Text;
using GenealogyCreator.Controls;
using System.Collections.ObjectModel;
using System.Resources;
using System.Reflection;

namespace GenealogyCreator.src
{
    public class Person : INDI
    {

        public string BirthName { get; set; }

        public ObservableCollection<MediaObject> MediaObjects { get; set; }

        public string Place { get; set; }

        private ResourceManager rm = new ResourceManager("GenealogyCreator.Properties.Resources", Assembly.GetExecutingAssembly());

        public bool isMale {
            get
            {
                return Sex == 'M';
            }
            set
            {
                if (value)
                    Sex = 'M';
                else
                    Sex = 'F';

            }
        }

        public bool isDead { get; set; }
        public string InfoText { get; set; }


        public int X { get; set; }
        public int Y { get; set; }

        #region from GEDNET
        public INDI i { get; private set; }

        public Family IsChildOf { get; set; }

        [NotMapped]
        public Guid? IsChildOfID { get; set; }

        public virtual ICollection<Family> Spouses { get; set; }

        [NotMapped]
        public bool isArranged { get; set; }
        #endregion

        public HashSet<PersonLink> Links { get; set; }

        [NotMapped]
        public string InfoLine {
            get {
                return getInfo().Replace("\n", "");
            }
            private set { }
        }


        //Non-Persistent Data
        [NotMapped]
        public virtual Thickness PanelPosition
        {
            get
            {
                return new Thickness(X, Y, 0, 0);
            }
            set
            {
                X = (int)value.Left;
                Y = (int)value.Top;
            }
        }
        [NotMapped]
        protected ThreeLine MothersLine { get; set; }
        [NotMapped]
        protected Line Line { get; set; }
        [NotMapped]
        protected List<Person> Children { get; set; }
        [NotMapped]
        public virtual Controls.PersonPanel itsPanel { get; set; }
        [NotMapped]
        public virtual bool spouseLineDrawn { get; set; }
        [NotMapped]
        public virtual bool parentLineDrawn { get; set; }


        [NotMapped]
        public string GenderString { get { return (isMale ? rm.GetString("Male") : rm.GetString("Female"));  } private set { } }

        
        public Person()
        {
            Spouses = new List<Family>();
            MediaObjects = new ObservableCollection<MediaObject>();
            spouseLineDrawn = false;
            parentLineDrawn = false;
            FirstName = rm.GetString("Firstname");
            LastName = rm.GetString("Lastname");
            Links = new HashSet<PersonLink>();

            ID = Guid.NewGuid();
        }

        public Person(INDI i)
        {
            Links = new HashSet<PersonLink>();
            spouseLineDrawn = false;
            parentLineDrawn = false;
            Spouses = new List<Family>();
            MediaObjects = new ObservableCollection<MediaObject>();

            this.i = i;
            ID = Guid.NewGuid();

            gedcomID = i.gedcomID;
            Changed = i.Changed;
            Sex = i.Sex;
            gedcomID = i.gedcomID;
            LastName = i.LastName;
            FirstName = i.FirstName;
            Phone = i.Phone;

            Address = new ADDR(i.Address);

            foreach (EVENT e in i.Events)
                Events.Add(new EVENT(e));

            foreach (NOTE e in i.Notes)
                Notes.Add(new NOTE(e));

            foreach (SOUR_USE e in i.Sources)
                Sources.Add(new SOUR_USE(e));

            foreach (OBJE e in i.Objects)
                Objects.Add(new OBJE(e));

        }

        public Person(Person refPerson, Family.Relationships relation)
        {
            spouseLineDrawn = false;
            parentLineDrawn = false;
            Spouses = new List<Family>();
            this.LastName = rm.GetString("name_of") + " " + refPerson.ToString();
            Links = new HashSet<PersonLink>();

            switch (relation)
            {
                //This person is a Mother/Father/Parent of refPerson
                case Family.Relationships.Mother:
                case Family.Relationships.Father:
                case Family.Relationships.Parent:
                    {
                        this.FirstName = rm.GetString("Parent");
                        Family refFam = refPerson.IsChildOf;

                        if (refFam == null)
                        {
                            refFam = new Family();
                            refPerson.IsChildOf = refFam;
                            refFam.Children.Add(refPerson);
                        }
                        this.PanelPosition = refPerson.PanelPosition;

                        Spouses.Add(refFam);
                        refFam.Spouses.Add(this);

                        if (relation == Family.Relationships.Father)
                            isMale = true;
                        else
                            isMale = false;
                    }
                    break;
                case Family.Relationships.Wife:
                case Family.Relationships.Husband:
                case Family.Relationships.Spouse:
                    this.FirstName = rm.GetString("Spouse");

                    Family commonFam = new Family();
                    commonFam.Spouses.Add(this);
                    commonFam.Spouses.Add(refPerson);

                    Spouses.Add(commonFam);
                    refPerson.Spouses.Add(commonFam);

                    if (relation == Family.Relationships.Husband)
                        isMale = true;
                    else
                        isMale = false;


                    break;
                case Family.Relationships.Daughter:
                case Family.Relationships.Son:
                case Family.Relationships.Children:
                    {
                        this.FirstName = rm.GetString("Child");
                        Family refFam = null;

                        //Check if refPerson is already married to someone,
                        //then use that first one Family
                        if (refPerson.Spouses.Count > 0)
                        {
                            refFam = (refPerson.Spouses as List<Family>)[0];
                        }
                        else
                        {
                            refFam = new Family();
                            refFam.Spouses.Add(refPerson);
                            refPerson.Spouses.Add(refFam);
                        }

                        IsChildOf = (refFam);
                        IsChildOfID = refFam.IDf;
                        refFam.Children.Add(this);

                        if (relation == Family.Relationships.Son)
                            isMale = true;
                        else
                            isMale = false;
                    }
                    break;
                case Family.Relationships.Sister:
                case Family.Relationships.Brother:
                case Family.Relationships.Sibling:
                    this.FirstName = rm.GetString("Spouse");


                    if (relation == Family.Relationships.Brother)
                        isMale = true;
                    else
                        isMale = false;

                    break;
                default:
                    break;
            }
        }


        public void AddLink(PersonLink p)
        {
            if (!Links.Contains(p))
                Links.Add(p);
        }

        public void RemoveLink(PersonLink p)
        {
            if (Links.Contains(p))
                Links.Remove(p);
        }

        public string getInfo()
        {
            string info = FirstName + " " + LastName;

            StringBuilder PersonInfoTxt = new StringBuilder();

            if (BirthName != null && BirthName.Length > 0)
                PersonInfoTxt.Append("( " + BirthName + " )\n");

            EVENT birth = getEvent(EVENT.TYPES.BIRTH);
            if (birth != null)
                PersonInfoTxt.Append("*" + birth.Date + " " + birth.Place + "\n");

            EVENT death = getEvent(EVENT.TYPES.DEATH);
            if (death != null)
                PersonInfoTxt.Append(" - " + death.Date + " " + death.Place + "\n");

            info = PersonInfoTxt.ToString();
            return FirstName + " " + LastName + "\n" + PersonInfoTxt.ToString();
            
        }


        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        internal ICollection<Person> getParents()
        {
            if (IsChildOf != null)
                return IsChildOf.Spouses;
            else
                return new List<Person>();
        }

        internal ICollection<Person> getChildren()
        {
            ICollection<Person> res = new List<Person>();

            foreach (var f in Spouses)
            {
                foreach (var c in f.Children)
                {
                    res.Add(c);
                }
            }

            return res;
        }

        internal ICollection<Person> getSpouses()
        {
            ICollection<Person> res = new List<Person>();

            foreach (var f in Spouses)
            {
                foreach (var c in f.Spouses)
                {
                    if(c != this)
                        res.Add(c);
                }
            }

            return res;
        }
    }
}
