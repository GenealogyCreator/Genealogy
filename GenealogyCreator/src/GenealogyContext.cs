﻿using GedNet;
using NLog;
using SQLite.CodeFirst;
using System;
using System.Data.Entity;
using System.Data.SQLite;

namespace GenealogyCreator.src
{
    public class GenealogyContext : DbContext
    {
        public DbSet<Source> Sources { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Family> Families { get; set; }
        public DbSet<MediaObject> Objects { get; set; }
        public DbSet<PersonLink> PersonLinks { get; set; }

        //public GenealogyContext()
        //: base("Data Source=" + Properties.Settings.Default.databaseFile) { }

        private Logger logger = LogManager.GetCurrentClassLogger();

        public GenealogyContext(string connectionString)
            : base(new SQLiteConnection() { ConnectionString = connectionString }, true)
        {

        }

        


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteDropCreateDatabaseWhenModelChanges<GenealogyContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);
            

            modelBuilder.Entity<Person>()
                        .HasOptional(s => s.Address);
            

            modelBuilder.Entity<Event>()
                        .HasOptional(s => s.Address);
            
            


            modelBuilder.Entity<Family>()
            .HasMany(i => i.Spouses)
            .WithMany(c => c.Spouses)
            .Map(cs =>
            {
                cs.MapLeftKey("FamilySRefId");
                cs.MapRightKey("PersonSRefId");
                cs.ToTable("PersonFamily");
            });

        }

        internal void TrySave()
        {
            try
            {
                SaveChanges();
            }
            catch (Exception e)
            {
                logger.Error("Error saving Databse: \n" + e);
                throw;
            }
        }
    }
}
