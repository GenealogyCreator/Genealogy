﻿using GedNet;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.Entity;
using PdfSharp.Drawing.Layout;

namespace GenealogyCreator.src
{
    class PDFCreator
    {
        private string outputFile;
        private GenealogyContext db;
        private List<Person> people;

        private double padding = 2;

        private double PageWidth_MM = 840;
        private const double EmToMm = 1.0 / (4.2175176);
        struct XPnt
        {
            public XUnit X;
            public XUnit Y;
        };

        private XPnt pageDim;
        private XUnit Factor;

        public PDFCreator(string filename)
        {
            this.outputFile = filename;
            db = new GenealogyContext(Properties.Settings.Default.db_connection_string);
            people = db.People.Include(X => X.Events).ToList<Person>();
        }

        public void createPDF()
        {
            Point dim = getDimesions();

            double factor =  ((double)Properties.Settings.Default.PDF_PageWidth) / dim.X;

            pageDim.X = XUnit.FromMillimeter(dim.X * factor);
            pageDim.Y = XUnit.FromMillimeter(dim.Y * factor);

            Factor = XUnit.FromMillimeter(factor);


            // Create a new PDF document
            PdfDocument document = new PdfDocument();
            document.Info.Title = "GenealogyCreator";

            // Create an empty page
            PdfPage page = document.AddPage();
            page.Width = pageDim.X;
            page.Height = pageDim.Y;
            
            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);
            double fontsizeMM = ((double)Properties.Settings.Default.PDF_FontSize) * Factor.Millimeter;
            XFont font = new XFont("Verdana", fontsizeMM, XFontStyle.Regular);
            XTextFormatter tf = new XTextFormatter(gfx);


            XPen pen = new XPen(XColors.Navy, .5);
            foreach (Person p in people)
            {
                XRect rect = new XRect(p.PanelPosition.Left * Factor.Point, 
                                        p.PanelPosition.Top * Factor.Point, 
                                        180 * Factor.Point, 
                                        60 * Factor.Point);


                XRect rectContent = new XRect(p.PanelPosition.Left * Factor.Point + padding, 
                                                p.PanelPosition.Top * Factor.Point + padding,
                                                250 * Factor.Point - padding, 
                                                60 * Factor.Point - padding);
                gfx.DrawRectangle(pen,  rect);
                

                StringBuilder PersonInfoTxt = new StringBuilder();

                PersonInfoTxt.Append(p.FirstName + " " + p.LastName + " \n");

                if (p.BirthName != null && p.BirthName.Length > 0)
                    PersonInfoTxt.Append("( " + p.BirthName + " )\n");


                EVENT birth = p.getEvent(GedNet.EVENT.TYPES.BIRTH);
                if (birth != null)
                    PersonInfoTxt.Append("*" + birth.Date + " " + birth.Place + "\n");

                EVENT death = p.getEvent(GedNet.EVENT.TYPES.DEATH);
                if (death != null)
                    PersonInfoTxt.Append("- " + death.Date + " " + death.Place + "\n");


                tf.DrawString(PersonInfoTxt.ToString(), font, XBrushes.Black, rectContent, XStringFormats.TopLeft);
                //DrawRectangle(gfx, p.PanelPosition);

            }

            try
            {
                document.Save(outputFile);
                Process.Start(outputFile);
            } catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void DrawRectangle(XGraphics gfx, Thickness panelPosition)
        {
        }

        private Point getDimesions()
        {
            Point max = new Point(0, 0);
            foreach (Person p in people)
            {
                if (p.PanelPosition.Left > max.X)
                    max.X = p.PanelPosition.Left;

                if (p.PanelPosition.Top > max.Y)
                    max.Y = p.PanelPosition.Top;
            }

            //ADD panelsize
            max.X += 200;
            max.Y += 80;

            return max;

        }
    }
}
