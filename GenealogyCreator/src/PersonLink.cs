﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GenealogyCreator.src
{
    public class PersonLink
    {
        [Key]
        public Guid ID { get; set; }

        public Person PersonLinked { get; set; }
        public MediaObject Image { get; set; }
        public Coordinates OriginPos { get; set; }
        public Coordinates InitialSize { get; set; }
        
        public PersonLink()
        {
            ID = Guid.NewGuid();
            PersonLinked = null;
        }


        public override string ToString()
        {
            return PersonLinked?.ToString() ?? "Not selected";
        }

    }
}
