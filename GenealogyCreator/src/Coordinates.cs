﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.src
{
    public class Coordinates
    {

        public double X { get; set; }
        public double Y { get; set; }

        [NotMapped]
        public double Width { get { return X; } set { X = value; } }

        [NotMapped]
        public double Height { get { return Y; } set { Y = value; } }

        public Coordinates()
        {

        }

        public override string ToString()
        {
            return "X: " + Math.Round(X, 3).ToString() + " Y: " + Math.Round(Y, 3).ToString();
        }


        public Coordinates(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        internal void Scale(double zoomFactor)
        {
            this.X *= zoomFactor;
            this.Y *= zoomFactor;
        }
    }
}
