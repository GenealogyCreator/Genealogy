# GenealogyCreator

With GenealogyCreator you are able to create big family trees.

With intuitiv drag'n'drop you can arrange all persons as you want.


A detailed persons view let you arrange all details to a certain person. 
Including events like birth, death and wedding, a free-text biography but you are
also able to add File like family photos and link other people on that file.

With the build-in PDF generator you can generate a handy PDF for sharing and printing.